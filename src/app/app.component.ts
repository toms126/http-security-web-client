import {Component, ViewEncapsulation} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  title = 'http-security-client';

  public tabPos = 0;

  public username = "";

  public password = "";

  public logs = [];

  public testDate = new Date();

  constructor(private http: HttpClient){}

  public clickTab(pos){
    this.tabPos = pos;
  }

  public clickClear(){
    this.logs = [];
  }

  private getToken(data){
    return data.access_token;
  }

  public httpRequest(){
    let uri = "http://3.26.80.89:1476//oauth/login";
    this.logs.push(
      {
        time: new Date(),
        state: 0,
        msg: "HTTP login request made to " + uri + " with username: " + this.username + " password: " + this.password
      }
    )
    let headers = new HttpHeaders()
      .set('Content-type', 'application/x-www-form-urlencoded; charset=utf-8');
    let body = new URLSearchParams();
    body.set('username', this.username);
    body.set('password', this.password);
    body.set('grant_type', 'password');
    this.http.post(uri, body.toString(), {headers: headers})
      .subscribe(
      data => {
        this.logs.push(
          {
            time: new Date(),
            state: 0,
            msg: "HTTP Server responded <span class=\"ok-msg\">OK</span> | Token: " + this.getToken(data)
          }
        )
      }, error => {
          this.logs.push(
            {
              time: new Date(),
              state: 1,
              msg: error.error.error_description
            }
          )
        }
    )
  }

  public httpsRequest(){
    let uri = "https://dx9734.blueorange.ai/oauth/login";
    this.logs.push(
      {
        time: new Date(),
        state: 0,
        msg: "HTTPS login request made to " + uri + " with username: " + this.username + " password: " + this.password
      }
    )
    let headers = new HttpHeaders()
      .set('Content-type', 'application/x-www-form-urlencoded; charset=utf-8')
      .set('Authorization', 'Basic bXktdHJ1c3RlZC1jbGllbnQtQmx1M09yYW5nMy0zc3NheTpzZWNyZXQtQmx1M09yYW5nMy0zc3NheQ==');
    let body = new URLSearchParams();
    body.set('username', this.username);
    body.set('password', this.password);
    body.set('grant_type', 'password');
    this.http.post(uri, body.toString(), {headers: headers})
      .subscribe(
        data => {
          this.logs.push(
            {
              time: new Date(),
              state: 0,
              msg: "HTTPS Server responded <span class=\"ok-msg\">OK</span> | Token: " + this.getToken(data)
            }
          )
        }, error => {
          this.logs.push(
            {
              time: new Date(),
              state: 1,
              msg: error.error.error_description
            }
          )
        }
      )
  }

  public enterEvent(event){
    if (event.key == "Enter"){
      if (this.tabPos == 0){
        this.httpRequest();
      } else {
        this.httpsRequest();
      }
    }
  }



}
